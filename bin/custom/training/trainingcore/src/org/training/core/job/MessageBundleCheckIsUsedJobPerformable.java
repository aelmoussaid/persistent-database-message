package org.training.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.core.job.utils.UsedProperty;
import org.training.core.model.MessageBundleCheckIsUsedCronJobModel;
import org.training.core.model.PersistentBDMessageModel;

import java.util.ArrayList;
import java.util.List;

public class MessageBundleCheckIsUsedJobPerformable extends AbstractJobPerformable<MessageBundleCheckIsUsedCronJobModel> {

    private static final Logger LOG = LoggerFactory.getLogger(MessageBundleCheckIsUsedJobPerformable.class);
    private final UsedProperty usedProperty = new UsedProperty();

    @Override
    public PerformResult perform(MessageBundleCheckIsUsedCronJobModel messageBundleCheckIsUsedCronJobModel) {
        long start = System.currentTimeMillis();
        final String querySelect = "SELECT {p.PK} FROM {PersistentBDMessage AS p}";
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(querySelect);

        SearchResult<PersistentBDMessageModel> result = flexibleSearchService.search(flexibleSearchQuery);
        List<PersistentBDMessageModel> persistentBDMessages = new ArrayList<>();
        LOG.info("[Started :: CronJob verification of message usage] #####################################################################################");
        result.getResult().forEach(message-> {
            boolean isUsed = usedProperty.isUsed(message.getKey());
            if (!message.getIsUsed().equals(isUsed)) {
                message.setIsUsed(isUsed);
                persistentBDMessages.add(message);
            }
        });
        modelService.saveAll(persistentBDMessages);
        long end = System.currentTimeMillis();
        LOG.info("[Finished :: CronJob verification of message usage | Elapsed time : {} ms] ##########################################",end - start);
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

}
