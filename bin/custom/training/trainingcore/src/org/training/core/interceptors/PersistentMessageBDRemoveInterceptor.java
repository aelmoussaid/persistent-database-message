package org.training.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import org.training.core.model.PersistentBDMessageModel;

public class PersistentMessageBDRemoveInterceptor implements RemoveInterceptor<PersistentBDMessageModel> {
    @Override
    public void onRemove(PersistentBDMessageModel persistentBDMessageModel, InterceptorContext interceptorContext) throws InterceptorException {
        if(persistentBDMessageModel.getIsUsed().equals(Boolean.TRUE)){
            throw new InterceptorException("Cannot remove value used in StoreFront");
        }
    }
}
