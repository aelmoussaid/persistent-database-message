package org.training.core.interceptors.validators;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.training.core.model.PersistentBDMessageModel;


public class PersistentMessageBDValidateInterceptor implements ValidateInterceptor<PersistentBDMessageModel> {

    @Override
    public void onValidate(PersistentBDMessageModel persistentBDMessageModel, InterceptorContext interceptorContext) throws InterceptorException {
          this.checkRequiredAttributes(persistentBDMessageModel);
    }

    private void checkRequiredAttributes(PersistentBDMessageModel persistentBDMessageModel) throws InterceptorException{
        if(persistentBDMessageModel.getIsFromBase().equals(Boolean.FALSE) && persistentBDMessageModel.getSite() == null && persistentBDMessageModel.getTheme() == null){
            throw new InterceptorException("Site and theme cannot be null if base attribute is false");
        }
    }

}
