package org.training.core.interceptors.preparators;


import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.training.core.model.PersistentBDMessageModel;
import org.training.core.services.AbstractPersistentBDMessageTranslator;


public class PersistentMessageBDPrepareInterceptor implements PrepareInterceptor<PersistentBDMessageModel> {

    private AbstractPersistentBDMessageTranslator persistentBDMessageTranslator;

    @Override
    public void onPrepare(PersistentBDMessageModel persistentBDMessage, InterceptorContext interceptorContext) {
        if (this.isMessageIndependantOfSiteAndTheme(persistentBDMessage)) {
            persistentBDMessage.setSite(null);
            persistentBDMessage.setTheme(null);
        }

        persistentBDMessageTranslator.startTranslation(persistentBDMessage,interceptorContext);

    }

    private boolean isMessageIndependantOfSiteAndTheme(PersistentBDMessageModel messageModel){
        return messageModel.getIsFromBase().equals(Boolean.TRUE) && messageModel.getSite() != null && messageModel.getTheme() != null;
    }

    public void setPersistentBDMessageTranslator(AbstractPersistentBDMessageTranslator persistentBDMessageTranslator) {
        this.persistentBDMessageTranslator = persistentBDMessageTranslator;
    }
}
