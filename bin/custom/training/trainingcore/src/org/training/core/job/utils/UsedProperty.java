package org.training.core.job.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UsedProperty {
    private final File directory ;
    private final List<String> files;

    public UsedProperty() {
        String filePath = UsedProperty.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        this.directory = new File(filePath.substring(0, filePath.indexOf("/custom/") + 8));
        this.files = filesUseProperties();
    }

    public boolean isUsed(String property) {
        return grep(property);
    }

    private boolean grep(String property){
        int total = 100;
        int size = files.size()/total;
        for(int i=0; i < total; i++) {
            MessageThread messageThread = new MessageThread(files.subList(i * size, (i + 1) * size), property);
            messageThread.run();
            if(messageThread.isUsed()) {
                return true;
            }
        }
        return false;
    }
    private List<String> filesUseProperties() {
        List<String> files = new ArrayList<>();

        try (Stream<Path> walk = Files.walk(directory.toPath())){
            List<String> result = walk.filter(Files::isRegularFile).map(Path::toString).collect(Collectors.toList());
            for (String file : result) {
                if (Arrays.stream(new String[]{"java", "jsp", "js", "tag"}).anyMatch(file::endsWith)) {
                    files.add(file);
                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
        return files;
    }
}
