package org.training.core.job.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Pattern;

public class MessageThread implements Runnable{
    private final List<String> files;
    private final String property;
    private boolean isUsed;

    public MessageThread(List<String> files, String property) {
        this.files = files;
        this.property = property;
    }
    @Override
    public void run() {
        for (String file : files) {
            try (BufferedReader br = new BufferedReader(Files.newBufferedReader(Paths.get(file)))) {
                String line;
                while ((line = br.readLine()) != null) {
                    if (Pattern.compile("(?<!\\\\S)\"" + property + "\"(?!\\\\S)").matcher(line).find()) {
                        isUsed = Boolean.TRUE;
                    }
                }
            } catch (IOException ignored) {}
        }
    }
    public boolean isUsed() {
        return isUsed;
    }
}
