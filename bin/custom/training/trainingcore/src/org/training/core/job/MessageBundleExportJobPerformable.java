package org.training.core.job;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.impex.jalo.ImpExManager;
import de.hybris.platform.impex.jalo.ImpExMedia;
import de.hybris.platform.impex.jalo.exp.*;
import de.hybris.platform.impex.jalo.exp.generator.HeaderLibraryGenerator;
import de.hybris.platform.impex.model.exp.ImpExExportMediaModel;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.impex.ImpExResource;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import org.apache.log4j.Logger;
import org.training.core.model.MessageBundleExportCronJobModel;
import org.training.core.model.PersistentBDMessageModel;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class MessageBundleExportJobPerformable extends AbstractJobPerformable<MessageBundleExportCronJobModel> {

    private static final Logger LOG = Logger.getLogger(MessageBundleExportJobPerformable.class);
    private TypeService typeService;


    @Override
    public PerformResult perform(MessageBundleExportCronJobModel cronJobModel) {


        final StringBuilder persistentBDHeaderBuilder = new StringBuilder();

        this.writeImpexHeader(cronJobModel.getIncludedLanguages(), persistentBDHeaderBuilder);
        this.writeFlexibleSearchQuery(cronJobModel.getIncludedSites(), persistentBDHeaderBuilder);
        this.startExport(cronJobModel, persistentBDHeaderBuilder);

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    public void setTypeService(TypeService typeService) {
        this.typeService = typeService;
    }

    private void startExport(MessageBundleExportCronJobModel cronJobModel, StringBuilder persistentBDHeaderBuilder) {
        try {
            Transaction.current().execute(new TransactionBody() {
                @Override
                public Object execute() throws Exception {
                    Date dateOfToday = new Date();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    final String dataExportZipName = "messageBundle-" + dateFormat.format(dateOfToday) + "-" + System.currentTimeMillis();

                    final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(persistentBDHeaderBuilder.toString().getBytes(StandardCharsets.UTF_8));
                    final ImpExResource impExResource = new StreamBasedImpExResource(byteArrayInputStream, StandardCharsets.UTF_8.displayName());
                    final ImpExMedia impexMedia = modelService.getSource(impExResource.getMedia());
                    final ExportConfiguration exportConfiguration = new ExportConfiguration(impexMedia, ImpExManager.getExportOnlyMode());

                    ImpExExportMedia destMedia = ExportUtils.createDataExportTarget(dataExportZipName);
                    exportConfiguration.setDataExportTarget(destMedia);

                    final Exporter exporter = new Exporter(exportConfiguration);
                    Export export = exporter.export();
                    MessageBundleExportJobPerformable.this.saveCronJobToExportedMedia(cronJobModel, export.getExportedData());
                    return null;
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void writeImpexHeader(Set<LanguageModel> includedLanguages, StringBuilder persistentBDHeaderBuilder) {
        final Set<ComposedType> types = new HashSet<>();
        final HeaderLibraryGenerator headerLibraryGenerator = new HeaderLibraryGenerator();
        ComposedTypeModel composedTypeModel = this.typeService.getComposedTypeForCode(PersistentBDMessageModel._TYPECODE);
        ComposedType composedType = this.modelService.getSource(composedTypeModel);

        types.add(composedType);
        Set<Language> languages = includedLanguages.stream().map(languageModel -> (Language) modelService.getSource(languageModel))
                .collect(Collectors.toSet());

        headerLibraryGenerator.setTypes(types);

        if (!includedLanguages.isEmpty()) {
            headerLibraryGenerator.setLanguages(languages);
        }

        headerLibraryGenerator.addIgnoreColumn(PersistentBDMessageModel._TYPECODE, "owner");
        headerLibraryGenerator.addIgnoreColumn(PersistentBDMessageModel._TYPECODE, "creationtime");
        headerLibraryGenerator.addIgnoreColumn(PersistentBDMessageModel._TYPECODE, "modifiedtime");

        String generatedImpexScript = headerLibraryGenerator.generateScript();
        persistentBDHeaderBuilder.append("INSERT_UPDATE ")
                .append(generatedImpexScript.substring(generatedImpexScript.indexOf(PersistentBDMessageModel._TYPECODE)));
    }

    private void writeFlexibleSearchQuery(Set<BaseSiteModel> includedSites, StringBuilder persistentBDHeaderBuilder) {
        persistentBDHeaderBuilder.append("\n" + "\"#%impex.exportItemsFlexibleSearch(\"\"SELECT {PK} FROM {PersistentBDMessage} ");
        if (!includedSites.isEmpty()) {
            String sitesSeparatedByComma = includedSites.stream().map(baseSiteModel -> baseSiteModel.getPk().toString())
                    .collect(Collectors.joining(","));
            persistentBDHeaderBuilder.append("WHERE {site} IN (").append(sitesSeparatedByComma).append(")");
        }
        persistentBDHeaderBuilder.append("\"\");\"");
    }

    private void saveCronJobToExportedMedia(MessageBundleExportCronJobModel cronJobModel, ImpExExportMedia exportedData) {
        ImpExExportMediaModel impExExportMediaModel = this.modelService.get(exportedData);
        impExExportMediaModel.setMessageBundleCronJob(cronJobModel);
        this.modelService.save(impExExportMediaModel);
        LOG.info("Media " + impExExportMediaModel.getCode() + " is added to cronjob " + cronJobModel.getCode() + " successfully!");
    }

}
