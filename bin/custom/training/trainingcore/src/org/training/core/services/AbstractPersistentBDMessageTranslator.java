package org.training.core.services;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.model.ItemModelContext;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.Config;
import org.apache.commons.codec.binary.StringUtils;
import org.training.core.model.PersistentBDMessageModel;

import java.util.*;

public abstract class AbstractPersistentBDMessageTranslator {
    protected FlexibleSearchService flexibleSearchService;

    private List<String> availableLanguagesIsoCodes = new ArrayList<>();

    protected abstract String translate(TranslationDataHolder translationDataHolder);

    public void startTranslation(PersistentBDMessageModel messageModel, InterceptorContext interceptorContext) {
        this.getLanguagesIsoCodes(messageModel.getSite());
        Locale sourceLocale = this.getLocaleForValueToBeUsedForTranslation(messageModel);
        if (sourceLocale != null) {
            for (String languageIsoCode : availableLanguagesIsoCodes) {
                Locale locale = new Locale(languageIsoCode);
                boolean isMessageNewAndValueEmpty = interceptorContext.isNew(messageModel) && messageModel.getValue(locale) == null;
                if (!sourceLocale.equals(locale) && (isMessageNewAndValueEmpty || !interceptorContext.isNew(messageModel))) {
                    byte[] bytes = StringUtils.getBytesUtf8(messageModel.getValue(sourceLocale));
                    TranslationDataHolder translationDataHolder = new TranslationDataHolder(sourceLocale.getLanguage(),
                            locale.getLanguage(), StringUtils.newStringIso8859_1(bytes));
                    String messageValue = this.translate(translationDataHolder);
                    messageModel.setValue(messageValue, locale);
                }
            }
        }
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    protected String getTranslationApiURL() {
        return Config.getParameter("persistentBDMessage.translationapi");
    }
    private Locale getLocaleForValueToBeUsedForTranslation(PersistentBDMessageModel messageModel) {
        final ItemModelContext itemModelContext = messageModel.getItemModelContext();

        for (String langIsoCode : availableLanguagesIsoCodes) {
            Locale locale = new Locale(langIsoCode);
            String messageModelValue = messageModel.getValue(locale);
            String originalValue = !itemModelContext.isNew() ? itemModelContext.getOriginalValue(PersistentBDMessageModel.VALUE, locale):null;

            boolean isValueModified = !itemModelContext.isNew() && (messageModelValue != null && !messageModelValue.equals(originalValue));
            boolean isValueNewAndChanged = itemModelContext.isNew() && messageModelValue != null;

            if (isValueNewAndChanged || isValueModified) {
                return locale;
            }
        }

        return null;
    }

    /**
     * If siteModel is not null; fetches only its languages otherwise
     * fetch all languages of all Base Stores
     */
    private void getLanguagesIsoCodes(BaseSiteModel siteModel) {
        StringBuilder queryBuilder = new StringBuilder();
        Map<String, Object> queryMap = new HashMap<>();
        if (siteModel != null) {
            queryBuilder.append("SELECT DISTINCT {l.isocode} FROM {StoresForCMSSite AS s JOIN BaseStore2LanguageRel AS bl ON {s.target}={bl.source}" +
                    " JOIN Language AS l ON {bl.target}={l.pk}} WHERE {s.source}=?sitePK");
            queryMap.put("sitePK", siteModel.getPk());
        } else {
            queryBuilder.append("SELECT DISTINCT {l.isocode} FROM {BaseStore2LanguageRel AS bs JOIN Language AS l ON {bs.target}={l.pk}}");
        }
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(queryBuilder);
        flexibleSearchQuery.addQueryParameters(queryMap);
        flexibleSearchQuery.setResultClassList(List.of(String.class));
        SearchResult<String> result = flexibleSearchService.search(flexibleSearchQuery);
        availableLanguagesIsoCodes = result.getResult();
    }

    protected static class TranslationDataHolder {
        private final String source;
        private final String target;
        @JsonProperty("q")
        private final String textToBeTranslated;

        public TranslationDataHolder(String source, String target, String textToBeTranslated) {
            this.source = source;
            this.target = target;
            this.textToBeTranslated = textToBeTranslated;
        }

        public String getTarget() {
            return target;
        }

        public String getSource() {
            return source;
        }

        public String getTextToBeTranslated() {
            return textToBeTranslated;
        }
    }
}
