package org.training.core.services.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.core.services.AbstractPersistentBDMessageTranslator;

import java.io.IOException;
import java.util.HashMap;

public class PersistentMessageLibreTranslate extends AbstractPersistentBDMessageTranslator {

    private static final Logger LOG = LoggerFactory.getLogger(PersistentMessageLibreTranslate.class);

    @Override
    protected String translate(TranslationDataHolder translationDataHolder) {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            ObjectMapper objectMapper = new ObjectMapper();
            HttpPost httpPost = new HttpPost(this.getTranslationApiURL());

            StringEntity entity = new StringEntity(objectMapper.writeValueAsString(translationDataHolder));
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpPost);
            String responseJSON = EntityUtils.toString(response.getEntity());
            HashMap<String, Object> result = objectMapper.readValue(responseJSON, new TypeReference<>() {
            });
            return (String) result.get("translatedText");
        } catch (IOException e) {
            LOG.error("Error translating message from {} to {} due to {}", translationDataHolder.getSource()
                    , translationDataHolder.getTarget(), e.getMessage());
            return null;
        }
    }
}
