package org.training.core.interceptors.initializers;

import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.internal.model.ServicelayerJobModel;
import org.training.core.model.MessageBundleExportCronJobModel;

public class MessageBundleExportCronJobInitDefaultsInterceptor implements InitDefaultsInterceptor<MessageBundleExportCronJobModel> {
    private static final String MESSAGE_BUNDLE_EXPORT_JOB_PERFORMABLE = "messageBundleExportJobPerformable";
    private CronJobService cronJobService;

    @Override
    public void onInitDefaults(MessageBundleExportCronJobModel cronJobModel, InterceptorContext interceptorContext) {
        ServicelayerJobModel job;
        try {
            job = (ServicelayerJobModel) this.cronJobService.getJob(MESSAGE_BUNDLE_EXPORT_JOB_PERFORMABLE);
        } catch (UnknownIdentifierException e) {
            job = interceptorContext.getModelService().create(ServicelayerJobModel.class);
            job.setSpringId(MESSAGE_BUNDLE_EXPORT_JOB_PERFORMABLE);
            job.setCode(MESSAGE_BUNDLE_EXPORT_JOB_PERFORMABLE);
            interceptorContext.getModelService().save(job);
        }
        cronJobModel.setJob(job);
    }

    public void setCronJobService(CronJobService cronJobService) {
        this.cronJobService = cronJobService;
    }
}
