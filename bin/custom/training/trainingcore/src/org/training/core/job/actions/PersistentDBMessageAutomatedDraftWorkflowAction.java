package org.training.core.job.actions;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.workflow.jobs.AutomatedWorkflowTemplateJob;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.training.core.enums.WorkFlowStatusEnum;
import org.training.core.model.PersistentBDMessageModel;

import java.util.Set;
import java.util.stream.Collectors;

public class PersistentDBMessageAutomatedDraftWorkflowAction implements AutomatedWorkflowTemplateJob {
    private ModelService modelService;
    private PlatformTransactionManager transactionManager;

    @Override
    public WorkflowDecisionModel perform(WorkflowActionModel workflowActionModel) {
        (new TransactionTemplate(this.transactionManager)).execute(new TransactionCallbackWithoutResult() {
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                Set<PersistentBDMessageModel> pesistentMessages = workflowActionModel.getWorkflow().getAttachments().stream().map(item -> (PersistentBDMessageModel) item.getItem())
                        .collect(Collectors.toSet());
                pesistentMessages.forEach(message -> message.setWorkFlowStatus(WorkFlowStatusEnum.DRAFT));
                PersistentDBMessageAutomatedDraftWorkflowAction.this.modelService.saveAll(pesistentMessages);
            }
        });
        return workflowActionModel.getDecisions().stream().findFirst().orElse(null);
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public void setTransactionManager(PlatformTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }
}
