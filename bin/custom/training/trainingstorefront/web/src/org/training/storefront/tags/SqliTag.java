package org.training.storefront.tags;

import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.tags.MessageTag;

public class SqliTag extends MessageTag {
    private static final String MESSAGE_SOURCE_BEAN="persistentBDResourceBundleSource";

    @Override
    protected MessageSource getMessageSource(){
        ApplicationContext appContext = getRequestContext().getWebApplicationContext();
        return (MessageSource) appContext.getBean(MESSAGE_SOURCE_BEAN);
    }
}
