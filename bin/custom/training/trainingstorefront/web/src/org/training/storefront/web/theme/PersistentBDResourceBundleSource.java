package org.training.storefront.web.theme;


import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.training.core.enums.WorkFlowStatusEnum;
import org.training.core.model.PersistentBDMessageModel;
import org.training.storefront.util.SiteThemeResolverUtils;

import java.text.MessageFormat;
import java.util.*;

public class PersistentBDResourceBundleSource extends ReloadableResourceBundleMessageSource {

    private FlexibleSearchService flexibleSearchService;
    private SiteThemeResolverUtils siteThemeResolverUtils;

    @Override
    protected String resolveCodeWithoutArguments(String code, Locale locale) {
        return getMessageFromDB(code, locale);
    }

    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        String msg = getMessageFromDB(code, locale);

        if (msg != null) {
            return new MessageFormat(msg);
        }

        return null;
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    public void setSiteThemeResolverUtils(SiteThemeResolverUtils siteThemeResolverUtils) {
        this.siteThemeResolverUtils = siteThemeResolverUtils;
    }

    private String getMessageFromDB(String code, Locale locale) {
        final String SELECT = "SELECT {PK} FROM {PersistentBDMessage AS p LEFT OUTER JOIN BaseSite AS b ON {p.site}={b.pk} LEFT OUTER JOIN SiteTheme AS s ON {p.theme}={s.pk}}" +
                "WHERE {p.key}=?code AND {p.workFlowStatus}=?workFlowStatus AND ({b.uid}=?site OR ({b.uid}=?site AND {s.code}=?theme) OR {s.code}=?theme OR {p.isFromBase}=true)";

        Map<String, Object> params = new HashMap<>();
        params.put("code", code);
        params.put("site", getSiteName());
        params.put("theme", getThemeName());
        params.put("workFlowStatus", WorkFlowStatusEnum.DONE);

        FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(SELECT);
        flexibleSearchQuery.addQueryParameters(params);
        flexibleSearchQuery.setLocale(locale);

        SearchResult<PersistentBDMessageModel> results = flexibleSearchService.search(flexibleSearchQuery);
        List<PersistentBDMessageModel> listOfValues = results.getResult();
        if (!listOfValues.isEmpty()) {
            return getValueForSiteAndTheme(listOfValues);

        }

        return null;
    }

    private String getValueForSiteAndTheme(List<PersistentBDMessageModel> values) {
        PersistentBDMessageModel message = values.stream().filter(msg -> (msg.getSite() != null && msg.getTheme() != null)
                        && msg.getSite().getUid().equals(this.getSiteName()) && msg.getTheme().getCode().equals(this.getThemeName()))
                .findFirst().orElse(null);
        if (message == null) {
            return getValueForSite(values);
        }

        return message.getValue();
    }

    private String getValueForSite(List<PersistentBDMessageModel> values) {
        PersistentBDMessageModel message = values.stream().filter(msg -> msg.getSite() != null && msg.getSite().getUid().equals(getSiteName()))
                .findFirst().orElse(null);
        if (message == null) {
            return getValueForTheme(values);
        }

        return message.getValue();
    }

    private String getValueForTheme(List<PersistentBDMessageModel> values) {
        PersistentBDMessageModel messageModel = values.stream().filter(msg -> msg.getTheme() != null && msg.getTheme().getCode().equals(this.getThemeName()))
                .findFirst().orElse(null);
        if (messageModel == null) {
            return getValueForBase(values);
        }
        return messageModel.getValue();
    }

    private String getValueForBase(List<PersistentBDMessageModel> values) {
        PersistentBDMessageModel messageModel = values.stream().filter(msg -> msg.getIsFromBase().equals(Boolean.TRUE))
                .findFirst().orElse(null);
        if (messageModel == null) {
            return null;
        }
        return messageModel.getValue();
    }

    private String getSiteName() {
        return siteThemeResolver()[1];
    }

    private String getThemeName() {
        return siteThemeResolver()[2];
    }

    private String[] siteThemeResolver() {
        return siteThemeResolverUtils.resolveThemeForCurrentSite().split(",", 3);
    }


}
