package com.sqli.impex;

import com.sqli.models.MessageBundleImpex;
import com.sqli.models.WorkFlowStatus;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

public class FileToImpex {

    private final String filePath;
    private final FileMetaData fileMetaData;

    private String[] languages = new String[]{"en"} ;
    public FileToImpex(String filePath) {
        this.filePath = filePath;
        this.fileMetaData = new FileMetaData(this.filePath);
    }

    public List<MessageBundleImpex> getMessageBundleImpex(File file) throws IOException {
        String simpleFileName= fileMetaData.getSimpleFileName(file.getAbsolutePath());
        List<MessageBundleImpex> messageBundleImpexList = new ArrayList<>();
        Files.newBufferedReader(file.toPath()).lines().forEach(line -> {
            if(line.contains("=")){
                {
                    int index = line.indexOf("=");
                    String[] split = new String[]{line.substring(0, index), line.substring(index + 1)};
                    MessageBundleImpex messageBundleImpex= new MessageBundleImpex();
                    messageBundleImpex.setKey(split[0].trim());
                    Map<String, String> value = new HashMap<>();
                    value.put(fileMetaData.getLang(file.getName()),split[1]);
                    messageBundleImpex.setValue(value);
                    messageBundleImpex.setSite(fileMetaData.getSiteOfFile(simpleFileName));
                    messageBundleImpex.setTheme(fileMetaData.getThemeOfFile(simpleFileName));
                    messageBundleImpex.setFromBase(fileMetaData.isBaseFile(simpleFileName));
                    messageBundleImpexList.add(messageBundleImpex);
                }
            }
        });
        return messageBundleImpexList;
    }

    public List<MessageBundleImpex> getFilteredMessageBundle(Set<String> filesName) throws IOException {
        List<MessageBundleImpex> messageBundleImpexList = new ArrayList<>();
        for (String fileName : filesName) {
            File file = new File(fileName);
            messageBundleImpexList.addAll(getMessageBundleImpex(file));
        }
        List<MessageBundleImpex> messageBundles = new ArrayList<>();
        for (MessageBundleImpex messageBundleImpex : messageBundleImpexList) {
            MessageBundleImpex messageBundle  = messageBundles.stream().filter(item -> item.getKey().equals(messageBundleImpex.getKey()) &&
                    ((messageBundleImpex.getSite() == null || item.getSite().equals(messageBundleImpex.getSite()))
                            && (messageBundleImpex.getTheme() == null || item.getTheme().equals(messageBundleImpex.getTheme())))).findFirst().orElse(null);
            if(messageBundle == null){
                messageBundles.add(messageBundleImpex);
            }
            else {
                messageBundle.getValue().putAll(messageBundleImpex.getValue());
            }
        }
        messageBundles.stream().filter(item -> item.getValue().size() > 1).forEach(item -> item.setStatus(WorkFlowStatus.DONE));
        return messageBundles;
    }

    public List<MessageBundleImpex> getSiteMessageBundle(FileMetaData fileMetaData, String[] site, String[] languages) throws IOException {
        Set<String> filesName = fileMetaData.getSiteFilesName().stream()
                    .filter(fileName -> Arrays.stream(site).anyMatch(fileName::contains) && Arrays.stream(languages).anyMatch(fileName::contains))
                    .collect(Collectors.toSet());
        return getFilteredMessageBundle(filesName);
    }

    public List<MessageBundleImpex> getThemeMessageBundle(FileMetaData fileMetaData, String[] theme, String[] languages) throws IOException {
        Set<String> filesName = fileMetaData.getThemeFilesName().stream()
                .filter(fileName -> Arrays.stream(theme).anyMatch(fileName::contains) && Arrays.stream(languages).anyMatch(fileName::contains))
                .collect(Collectors.toSet());
        return getFilteredMessageBundle(filesName);
    }

    public List<MessageBundleImpex> getBaseMessageBundle(FileMetaData fileMetaData, String[] languages) throws IOException {
        Set<String> filesName = fileMetaData.getBaseFilesName();
        filesName.stream()
                .filter(fileName -> Arrays.stream(languages).anyMatch(fileName::contains))
                .collect(Collectors.toSet());
        return getFilteredMessageBundle(filesName);
    }
    public List<MessageBundleImpex> getMessageBundle(FileMetaData fileMetaData, String[] site, String[] theme, boolean base, String[] languages) throws IOException {
        List<MessageBundleImpex> messageBundles = new ArrayList<>();
        if (languages != null && !languages[0].isEmpty())
            this.languages = languages;
        if (site != null && !site[0].isEmpty()) {
            messageBundles = getSiteMessageBundle(fileMetaData, site, this.languages);
        }
        if (theme != null && !theme[0].isEmpty() ) {
            messageBundles.addAll(getThemeMessageBundle(fileMetaData, theme, this.languages));
        }
        if (base) {
            messageBundles.addAll(getBaseMessageBundle(fileMetaData, this.languages));
        }
        messageBundles.forEach(messageBundleImpex -> messageBundleImpex.print(languages));
        return messageBundles;
    }
    public List<MessageBundleImpex> convert(FileMetaData fileMetaData, String[] site, String[] theme, boolean base, String[] languages) throws IOException {
        return getMessageBundle(fileMetaData, site, theme, base, languages);
    }

}
