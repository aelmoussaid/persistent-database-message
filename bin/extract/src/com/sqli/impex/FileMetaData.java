package com.sqli.impex;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class FileMetaData{
    private final String filePath;
    private final File folder;
    private final Set<String> siteFilesName = new HashSet<>();
    private final Set<String> themeFilesName = new HashSet<>();
    private final Set<String> baseFilesName = new HashSet<>();

    public FileMetaData(String filePath) {
        this.filePath = filePath;
        this.folder = new File(this.filePath);
    }

    public String getSimpleFileName(String fileName) {
        int dotIndex = fileName.length() - ".properties".length();
        return fileName.endsWith(".properties") ? fileName.substring(filePath.length() + 1, dotIndex): null;
    }
    public Set<String> getListOfFileName() {
        Set<String> filesName = new HashSet<>();
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.isFile() && file.getName().endsWith(".properties")) {
                    filesName.add(file.getAbsolutePath());
                }
            }
        }
        return filesName;
    }
    public String getSiteOfFile(String fileName) {
        if (fileName.startsWith("site-")) {
            return fileName.substring(5, fileName.length() - 3);
        } else {
            return null;
        }
    }
    public boolean isSiteFile(String fileName) {
        return fileName.startsWith("site-");
    }
    public String getThemeOfFile(String fileName) {
        if (fileName.startsWith("theme-")) {
            return fileName.substring(6, fileName.indexOf("-", 6));
        } else {
            return null;
        }
    }

    public boolean isThemeFile(String fileName) {
        return fileName.startsWith("theme-");
    }
    public boolean isBaseFile(String fileName) {
        return fileName.startsWith("base");
    }
    public String getLang(String fileName) {
        int index = fileName.lastIndexOf(".");
        return fileName.substring(index - 2, index);
    }
    public  Set<String> getSiteFilesName() {
        for (String file : getListOfFileName()) {
            if (isSiteFile(getSimpleFileName(file))) {
                siteFilesName.add(file);
            }
        }
        return siteFilesName;
    }
    public Set<String> getThemeFilesName() {
        for (String file : getListOfFileName()) {
            if (isThemeFile(getSimpleFileName(file))) {
                themeFilesName.add(file);
            }
        }
        return themeFilesName;
    }
    public Set<String> getBaseFilesName() {
        for (String file : getListOfFileName()) {
            if (isBaseFile(getSimpleFileName(file))) {
                baseFilesName.add(file);
            }
        }
        return baseFilesName;
    }
}
