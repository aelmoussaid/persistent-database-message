package com.sqli.extract;

import com.sqli.impex.FileMetaData;
import com.sqli.impex.FileToImpex;
import com.sqli.models.MessageBundleImpex;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

public class Extract {
    private static final StringBuilder header = new StringBuilder("INSERT_UPDATE PersistentBDMessage;key[unique=true];");
    private static String filePath;
    private static String[] site;
    private static String[] theme;
    private static boolean base;
    private static String[] languages;


    private final FileMetaData fileMetaData;
    private final FileToImpex fileToImpex;

    public Extract(String filePath, String[] site, String[] theme, boolean base, String[] languages) {
        Extract.filePath = filePath;
        Extract.site = site;
        Extract.theme = theme;
        Extract.base = base;
        Extract.languages = languages;
        this.fileMetaData = new FileMetaData(filePath);
        this.fileToImpex = new FileToImpex(Extract.filePath);
    }

    public static Extract prepare(String[] args) {
        if (args.length < 5) {
            System.out.println("Please try with this form: java -jar extract.jar <filePath>[the path of the messages directory] <site>[-site=[site1],]] <theme>[-theme=[theme],]] <base>[-base=[true|false]] <languages>[-languages=[lang1,]]");
            System.exit(1);
        }
        filePath = args[0];
        site = (args[1].substring("-site=".length()).isEmpty() ? null : args[1].substring("-site=".length()).split(","));
        theme = (args[2].substring("-theme=".length()).isEmpty() ? null : args[2].substring("-theme=".length()).split(","));
        base = Boolean.parseBoolean(args[3].substring("-base=".length()));
        languages = (args[4].substring("-lang=".length()).isEmpty() ? null : args[4].substring("-lang=".length()).split(","));
        if (languages != null) {
            for (String lang : languages) {
                header.append("value[lang=").append(lang).append("];");
            }
        }
        else {
            header.append("value[lang=en]").append(";");
        }
        header.append("site(uid)[unique=true];theme(code)[unique=true];isFromBase;isUsed;workFlowStatus(code);");
        return new Extract(filePath, site, theme , base, languages);
    }
    public void extract() throws IOException {
        List<MessageBundleImpex> messageBundles= this.fileToImpex.convert(this.fileMetaData, site, theme , base, languages);
        try {
            File messageBundleImpex = new File("message-bundle.impex");
            RandomAccessFile raf = new RandomAccessFile(messageBundleImpex, "rwd");
            System.out.println("Start Extraction");
            System.out.println("You will find the output impex file in the following location :"
                            + messageBundleImpex.getCanonicalPath());
            raf.write(header.toString().getBytes());
            raf.write("\n".getBytes());
            for (MessageBundleImpex messageBundle : messageBundles) {
                raf.write(messageBundle.print(languages).getBytes());
                raf.write("\n".getBytes());
            }
            System.out.println("Extract done");
            raf.close();
        } catch (Exception e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        Extract extract = prepare(args);
        extract.extract();
        long end = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (end - start) + " ms");

    }

}
