package com.sqli.models;

import com.sun.istack.internal.Nullable;

import java.util.Arrays;
import java.util.Map;

public class MessageBundleImpex {
    private String key;
    private Map<String, String> value;
    @Nullable
    private String site;
    @Nullable
    private String theme;

    private boolean isFromBase;

    private boolean isUsed;
    private WorkFlowStatus status;

    public MessageBundleImpex() {
        this.isUsed = false;
        this.status = WorkFlowStatus.CREATED;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Map<String, String> getValue() {
        return value;
    }

    public void setValue(Map<String, String> value) {
        this.value = value;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
    public void setFromBase(boolean isFromBase) {
        this.isFromBase = isFromBase;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }
    public void setStatus(WorkFlowStatus status) {
        this.status = status;
    }
    public String print(String[] languages) {
        StringBuilder s = new StringBuilder(';' + key + ';');
        for (String lang : languages) {
            if (value.get(lang) == null)
                s.append(';');
            else if (value.get(lang).contains(";"))
                s.append('"').append(value.get(lang).trim()).append('"').append(';');
            else if (value.get(lang).contains("\""))
                s.append(value.get(lang).replace("\"", "\\\"").trim()).append(';');
            else
                s.append(value.get(lang).trim()).append(';');
        }
        s.append(site == null ? "" : site).append(';').append(theme == null ? "" : theme).append(';').append(isFromBase).append(';').append(isUsed).append(';').append(status).append(';');
        return s.toString();
    }
}

