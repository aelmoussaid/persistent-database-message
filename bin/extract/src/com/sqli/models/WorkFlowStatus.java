package com.sqli.models;

public enum WorkFlowStatus {

    DRAFT, CREATED, DONE
}