# Persistent Database Message



## About The Project

An SAP E-Commerce extension that enables management of message resources from the BackOffice.

## Getting Started

### Prerequisites

* An existing SAP E-commerce(Hybris) root folder.

### Installation
1. Get to hybris root folder.
2. If there is already a local git repository skip to step 3 otherwise execute command
   ```
   git init
   ```
3. Add remote to git
    ```
    git remote add origin https://gitlab.com/aelmoussaid/persistent-database-message.git
   ```
4. Download contents from remote repository
    ```
    git pull origin main 
    git branch -M master main 
    ```
5. Build project with following command
    ```
    ant all
    ```
6. Initialize SAP E-commerce(Hybris) via **HAC** platform.